from sqlalchemy import Column, Integer, String

from database import Base

class Articles(Base):
    __tablename__="articles"    
    id = Column(Integer, primary_key=True, index=True)
    title=Column(String)
    content=Column(String)

